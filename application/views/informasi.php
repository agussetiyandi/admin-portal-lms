<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Portal LMS - BP Batam</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="author">
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/header.php") ?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="page-title-box">
					<div class="row align-items-center">
						<div class="col-sm-6">
							<h4 class="page-title">Informasi</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:void(0);">Informasi</a></li>
								<!-- <li class="breadcrumb-item"><a href="javascript:void(0);">Tables</a></li> -->
								<li class="breadcrumb-item active">Data Informasi</li>
							</ol>
						</div>
						<div class="col-sm-6">
							<div class="float-right d-none d-md-block">
								<div id="add-button">
									<a href="<?= base_url('AddInformasi') ?>" class="btn btn-success mdi mdi-plus mr-2"> Tambah
										Informasi</a>
								</div>
							</div>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-12">
						<div class="card">
							<div class="card-body">
								<table id="datatable" class="table table-bordered dt-responsive nowrap"
									style="border-collapse: collapse; border-spacing: 0; width: 100%;">
									<thead>
										<tr>
											<th width="20px" style="text-align:center">No</th>
											<th style="text-align:center">Judul Informasi</th>
											<th style="text-align:center">Keterangan</th>
											<th style="text-align:center">Tanggal Input</th>
											<th style="text-align:center">Status</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td style="text-align:center">1</td>
											<td>Kunjungan Presiden</td>
											<td>Pada tanggal 17 Oktober 2019 presiden datang mengunjungi ...more</td>
											<td>18/19/2019</td>
											<td><a href="#" id="inline-status" data-type="select" data-pk="1" data-value="0"
													data-source="/status" data-title="Select status">Active</a></td>
											<td>
												<div id="add-button">
													<a href="<?= base_url('informasi/edit') ?>"
														class="btn btn-outline-warning mdi dripicons-document-edit mr-2"></a>
												</div>
											</td>
										</tr>
										<tr>
											<td style="text-align:center">2</td>
											<td>Kunjungan Presiden</td>
											<td>Pada tanggal 17 Oktober 2019 presiden datang mengunjungi ...more</td>
											<td>18/19/2019</td>
											<td><a href="#" id="inline-status" data-type="select" data-pk="1" data-value="0"
													data-source="/status" data-title="Select status">Active</a></td>
											<td>
												<div id="add-button">
													<a href="<?= base_url('informasi/edit') ?>"
														class="btn btn-outline-warning mdi dripicons-document-edit mr-2"></a>
												</div>
											</td>
										</tr>
										<tr>
											<td style="text-align:center">3</td>
											<td>Kunjungan Presiden</td>
											<td>Pada tanggal 17 Oktober 2019 presiden datang mengunjungi ...more</td>
											<td>18/19/2019</td>
											<td><a href="#" id="inline-status" data-type="select" data-pk="1" data-value="0"
													data-source="/status" data-title="Select status">Active</a></td>
											<td>
												<div id="add-button">
													<a href="<?= base_url('informasi/edit') ?>"
														class="btn btn-outline-warning mdi dripicons-document-edit mr-2"></a>
												</div>
											</td>
										</tr>

									</tbody>
								</table>
							</div>
						</div>
					</div><!-- end col -->
				</div><!-- end row -->

			</div><!-- container-fluid -->
		</div><!-- content -->
		<?php $this->load->view("_partials/footer.php") ?>
	</div><!-- ============================================================== -->
	<!-- End Right content here -->
	<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
