<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
	<title>Portal LMS - BP Batam</title>
	<meta content="Admin Dashboard" name="description">
	<meta content="Themesbrand" name="author">
	<link rel="shortcut icon" href="assets/images/favicon.ico">

	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
	<?php $this->load->view("_partials/header.php") ?>
	<!-- ============================================================== -->
	<!-- Start right Content here -->
	<!-- ============================================================== -->
	<div class="content-page">
		<!-- Start content -->
		<div class="content">
			<div class="container-fluid">
				<div class="page-title-box">
					<div class="row align-items-center">
						<div class="col-sm-6">
							<h4 class="page-title">Form FAQ</h4>
							<ol class="breadcrumb">
								<li class="breadcrumb-item"><a href="javascript:void(0);">Home</a></li>
								<li class="breadcrumb-item"><a href="javascript:void(0);">Form FAQ</a></li>
								<li class="breadcrumb-item active">Tambah FAQ</li>
							</ol>
						</div>
					</div>
				</div><!-- end row -->
				<div class="row">
					<div class="col-lg-12">
						<div class="card">
							<div class="card-body">
								<form class="" action="#">
									<div class="form-group"><label>Kategori</label>
										<form>
											<div class="form-group row">
												<div class="col-sm-12"><select class="form-control">
														<option>Umum</option>
														<option>Perijinan</option>
														<option>Peraturan</option>
														<option>Pendaftaran</option>
													</select></div>
											</div>
											<div class="form-group"><label>Pertanyaan</label>
												<div class="summernote"></div>
												<br>
												<div class="form-group"><label>Jawaban</label>
													<div class="summernote"></div>
													<br>

													<div class="form-group"><label>Set Aktif</label>
														<div><input type="checkbox" id="switch4" switch="success" checked="checked"> <label
																for="switch4" data-on-label="Aktif" data-off-label="Tidak"></label></div>
														<div class="form-group mb-0">
															<div><button type="submit"
																	class="btn btn-primary waves-effect waves-light mr-1">Submit</button>
																<button type="reset" class="btn btn-secondary waves-effect">Cancel</button></div>
														</div>
													</div>
										</form>
									</div>
							</div>
						</div><!-- end col -->
					</div><!-- end row -->
				</div><!-- container-fluid -->
			</div><!-- content -->
			<?php $this->load->view("_partials/footer.php") ?>
		</div><!-- ============================================================== -->
		<!-- End Right content here -->
		<!-- ============================================================== -->
	</div><!-- END wrapper -->
	<?php $this->load->view("_partials/js.php") ?>

</body>

</html>
