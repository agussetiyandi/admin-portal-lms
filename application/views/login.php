<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width,initial-scale=1,user-scalable=0,minimal-ui">
  <title>Login - Portal LSM BP Batam</title>
  <meta content="Admin Dashboard" name="description">
  <meta content="Themesbrand" name="author">
  <link rel="shortcut icon" href="assets/images/favicon.ico">

	<?php $this->load->view("_partials/css.php") ?>
</head>

<body>
  <div class="home-btn d-none d-sm-block"><a href="#" class="text-white"><i class="fas fa-home h2"></i></a>
  </div><!-- Begin page -->
  <div class="accountbg"></div>
  <div class="wrapper-page account-page-full">
    <div class="card">
      <div class="card-body">
        <div class="text-center"><a href="index.html" class="logo"><img src="assets/images/logo-light.png" height="55"
              alt="logo"></a></div>
        <div class="p-3">
          <h4 class="font-18 m-b-5 text-center">Selamat Datang!</h4>
          <p class="text-muted text-center">Login Portal LSM BP Batam</p>
          <form class="form-horizontal m-t-30" action="#">
            <div class="form-group"><label for="username">Username</label> <input type="text" class="form-control"
                id="username" placeholder="Enter username"></div>
            <div class="form-group"><label for="userpassword">Password</label> <input type="password"
                class="form-control" id="userpassword" placeholder="Enter password"></div>
            <div class="form-group row m-t-20">
              <div class="col-sm-6">
              </div>
              <div class="col-sm-6 text-right"><button class="btn btn-primary w-md waves-effect waves-light"
                  type="submit">Log In</button></div>
            </div>
            <div class="form-group m-t-10 mb-0 row">
            </div>
          </form>
        </div>
      </div>
    </div>
    <div class="m-t-40 text-center">

      <p>© 2020 Portal LSM BP Batam. All Right Reserved</p>
    </div>
  </div><!-- end wrapper-page -->
  <!-- jQuery  -->

  <?php $this->load->view("_partials/js.php") ?>
</body>

</html>